# US - Swedish keyboard layots

Coders and terminal hackers, who prefer the US keyboard layot but also need to type the swedish characters ÅÄÖåäö: this keyboard layots are for you! Instead of shifting between swedish and US keyboard layots all the time, you use this keyboard layot, which is basically the US one, but with AltGr+\[, AltGr+\' and AltGr+; you reach the å, ä and ö respectively. Add shift, and you get the upper case characters Å, Ä and Ö. The keys are located where you're used to have åäö on the normal swedish keyboard layots, which makes it easy to rewire your brain to use them.

Source files are released under GPLv3, see [LICENSE.md](./LICENSE.md). 
The MS Keyboard Layout Creator is proprietary.

## Debian and friends (e.g. Ubuntu, Mint)

Use the file in the debian folder 
(more instructions to come...)

## Windows

If your computer language is swedish, use the sv-us keyboard layot.

If your computer language is english, use the us-sv keyboard layot.

### Build from source

1. Get the [MS Keyboard Layout Creator](https://www.microsoft.com/en-us/download/details.aspx?id=22339).
2. Open the us-sv.klc or sv-us.klc file with MS Keyboard Layout Creator
3. Select Project->Build DLL and Setup Package

### Install prebuild msi-packages

An alternative is to run the prebuild `setup.exe` in the bin folder.

## Mac

Looking for a solution ... Please tell me: us-sv-keyboard@prograde.se

## Todo

* Add support for é
* Setup instructions
* Find a solution for mac
